plugins {
    id("easyweather.android.library")
    id("easyweather.android.hilt")
}

@Suppress("UnstableApiUsage")
android {
    namespace = "com.exalt.core.domain"

    defaultConfig {
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    testOptions {
        unitTests.isReturnDefaultValues = true
    }

}
// Allow references to generated code
kapt {
    correctErrorTypes = true
}

dependencies {

    implementation(libs.androidx.core.ktx)

    // Coroutines
    implementation(libs.kotlinx.coroutines.android)

    //Tests
    implementation(libs.kotlinx.coroutines.test)
    implementation(libs.bundles.unit.tests)

    // Time
    implementation(libs.joda)

}