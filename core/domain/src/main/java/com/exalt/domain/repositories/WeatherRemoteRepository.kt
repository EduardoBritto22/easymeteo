package com.exalt.domain.repositories

import com.exalt.domain.models.City
import com.exalt.domain.models.TimeStampWeatherData
import com.exalt.domain.models.WeatherData
import kotlinx.coroutines.flow.Flow

interface WeatherRemoteRepository {

     fun getCurrentWeatherBy(city: City): Flow<Result<WeatherData>>

     fun getTimeStampWeatherBy(city: City, timeStampCount: Int): Flow<Result<List<TimeStampWeatherData>>>

}