package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.repositories.CityLocalRepository
import javax.inject.Inject

class DeleteCityUseCase @Inject constructor(
    private val repository: CityLocalRepository
) {

    suspend operator fun invoke(city: City): Result<Unit> = runCatching{
        repository.remove(city)
    }
}