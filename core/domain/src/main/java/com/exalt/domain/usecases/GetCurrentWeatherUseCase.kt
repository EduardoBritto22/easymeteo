package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.models.WeatherData
import com.exalt.domain.repositories.WeatherRemoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class GetCurrentWeatherUseCase @Inject constructor(
    private val repository: WeatherRemoteRepository
){
    operator fun invoke(city: City): Flow<Result<WeatherData>> = runCatching {
            repository.getCurrentWeatherBy(city)
        }.getOrElse {
            flowOf(Result.failure(it))
    }
}