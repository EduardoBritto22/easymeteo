package com.exalt.domain.models

data class MainWeather(
    val mainWeatherDescription: String,
    val mainWeatherGroup: String,
    val mainWeatherIcon: String
)