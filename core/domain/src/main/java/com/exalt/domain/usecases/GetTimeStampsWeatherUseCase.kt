package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.models.TimeStampWeatherData
import com.exalt.domain.repositories.WeatherRemoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class GetTimeStampsWeatherUseCase @Inject constructor(
    private val repository: WeatherRemoteRepository
){
    operator fun invoke(city: City, nbOfTimeStamps: Int): Flow<Result<List<TimeStampWeatherData>>> = runCatching {
        repository.getTimeStampWeatherBy(city,nbOfTimeStamps)
    }.getOrElse {
        flowOf(Result.failure(it))
    }
}