package com.exalt.domain.repositories

import com.exalt.domain.models.City
import kotlinx.coroutines.flow.Flow

interface CityLocalRepository {

    fun getCitiesList(): Flow<List<City>>

    suspend fun save(city:City)
    suspend fun remove(city: City)

}