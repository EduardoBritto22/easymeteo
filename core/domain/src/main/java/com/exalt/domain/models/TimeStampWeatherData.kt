package com.exalt.domain.models

import org.joda.time.LocalDateTime

data class TimeStampWeatherData(
    val timeStamp: LocalDateTime,
    val temperature: Double,
    val humidity: Int,
    val mainWeatherIcon: String,
    val windDirectionDeg: Int,
    val windSpeed: Double,
    val precipitationProbability: Double
)
