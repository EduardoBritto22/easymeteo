package com.exalt.domain.models

import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime

data class City(
    val id: Int,
    var lat: Double? = null,
    var lon: Double? = null,
    val name: String,
    val stateCode: String?,
    val countryCode: String,
    var sunrise: LocalDateTime? = null, //Todo: Get the sunrise, sunset and time zone  to be able to show them to the user
    var sunset: LocalDateTime? = null,
    var timezone: DateTimeZone? = null
)