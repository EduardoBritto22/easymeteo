package com.exalt.domain.models

import org.joda.time.LocalDateTime

data class WeatherData(
    val id: Int,
    val temperature: Double,
    val feelsLike: Double,
    val humidity: Int,
    val mainWeather: List<MainWeather>,
    val time: LocalDateTime,
    val windDirectionDeg: Int,
    val windSpeed: Double
)

