package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.repositories.CityLocalRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

class GetCitiesListUseCase @Inject constructor(
    private val repository: CityLocalRepository
){

    operator fun invoke(): Flow<List<City>> = runCatching {
            repository.getCitiesList()
        }.getOrElse {
            flowOf(emptyList())
        }

}