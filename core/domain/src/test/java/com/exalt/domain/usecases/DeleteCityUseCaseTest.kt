package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.repositories.CityLocalRepository
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class DeleteCityUseCaseTest {

    @Mock
    private lateinit var repository: CityLocalRepository
    private lateinit var deleteCityUseCase: DeleteCityUseCase


    private val sampleCitiesList = mutableListOf(

        City(
            id = 1,
            name = "Belo Horizonte",
            stateCode = "MG",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        City(
            id = 2,
            name = "Paris",
            stateCode = null,
            countryCode = "FR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        City(
            id = 3,
            name = "London",
            stateCode = null,
            countryCode = "UK",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        City(
            id = 4,
            name = "São Paulo",
            stateCode = "SP",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
    )



    @Before
    fun setUp(){
        deleteCityUseCase = DeleteCityUseCase(repository)
    }

    @Test
    fun `Given local repository deletes the city, When DeleteCityUseCase is invoked, Then returns success and the city is removed`() = runTest {

        // Given a mocked local repository that removes a city from the list, injected into the use case..
        Mockito.`when`(repository.remove(any())).then {
            sampleCitiesList.remove(it.arguments[0])
        }
        val listSizeExpected = sampleCitiesList.size - 1

        // When when we save a city in the local repository.
        val citySaved = deleteCityUseCase(City(
            id = 1,
            name = "Belo Horizonte",
            stateCode = "MG",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ))

        // Then the result should be true
        assertTrue(citySaved.isSuccess)
        //Then the list must have a city removed
        assertEquals(listSizeExpected, sampleCitiesList.size)
    }

    @Test
    fun `Given local repository throws an exception , When DeleteCityUseCase is invoked, Then returns failure`() = runTest {
        // Given a mocked local repository that throws an exception, injected into the use case..
        Mockito.`when`(repository.remove(any())).thenThrow(RuntimeException())

        // When we delete a city from the local repository.
        val cityDeleted = deleteCityUseCase(City(
            id = 1,
            name = "Belo Horizonte",
            stateCode = "MG",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ))

        // Then the result should be false
        assertTrue(cityDeleted.isFailure)
    }



}