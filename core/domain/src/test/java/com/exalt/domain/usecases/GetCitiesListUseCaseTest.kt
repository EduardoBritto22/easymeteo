package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.repositories.CityLocalRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetCitiesListUseCaseTest {

    @Mock
    private lateinit var repository: CityLocalRepository
    private lateinit var getCitiesListUseCase: GetCitiesListUseCase

    @Before
    fun setUp(){
        getCitiesListUseCase = GetCitiesListUseCase(repository)
    }

    @Test
    fun `Given local repository returns full list, When GetCitiesList is invoked, Then returns full list`() = runTest {
        // Given a mocked local repository injected into the use case..
       Mockito.`when`(repository.getCitiesList()).thenReturn(flowOf(sampleCitiesList))

        // When when the list of cities is returned from the use case...
        val citiesList = getCitiesListUseCase()

        // Then the result should be the sample list
        assertEquals(sampleCitiesList, citiesList.first())
    }

    @Test
    fun `Given local repository throws an exception , When GetCitiesList is invoked, Then returns empty list`() = runTest {
        // Given a mocked local repository that throws an exception, injected into the use case..
        Mockito.`when`(repository.getCitiesList()).thenThrow(RuntimeException())

        // When when the list of cities is returned from the use case...
        val citiesList = getCitiesListUseCase()

        // Then the result should be an empty list
        assertEquals(emptyList<City>(), citiesList.first())
    }


    private val sampleCitiesList = listOf(

        City(
            id = 1,
            name = "Belo Horizonte",
            stateCode = "MG",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        City(
            id = 2,
            name = "Paris",
            stateCode = null,
            countryCode = "FR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        City(
            id = 3,
            name = "London",
            stateCode = null,
            countryCode = "UK",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        City(
            id = 4,
            name = "São Paulo",
            stateCode = "SP",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
    )


}