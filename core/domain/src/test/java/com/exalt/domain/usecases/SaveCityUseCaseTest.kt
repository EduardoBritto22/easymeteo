package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.repositories.CityLocalRepository
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any

@RunWith(MockitoJUnitRunner::class)
class SaveCityUseCaseTest {

    @Mock
    private lateinit var repository: CityLocalRepository
    private lateinit var saveCityUseCase: SaveCityUseCase

    @Before
    fun setUp(){
        saveCityUseCase = SaveCityUseCase(repository)
    }

    @Test
    fun `Given local repository returns saves the city, When SaveCityUseCase is invoked, Then returns success`() = runTest {

        // When when we save a city in the local repository.
        val citySaved = saveCityUseCase(City(
            id = 1,
            name = "Belo Horizonte",
            stateCode = "MG",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ))

        // Then the result should be true
        assertTrue(citySaved.isSuccess)
    }

    @Test
    fun `Given local repository throws an exception , When SaveCityUseCase is invoked, Then returns failure`() = runTest {
        // Given a mocked local repository that throws an exception, injected into the use case..
        Mockito.`when`(repository.save(any())).thenThrow(RuntimeException())

        // When we save a city in the local repository.
        val citySaved = saveCityUseCase(City(
            id = 1,
            name = "Belo Horizonte",
            stateCode = "MG",
            countryCode = "BR",
            lat = null,
            lon = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ))

        // Then the result should be false
        assertTrue(citySaved.isFailure)
    }



}