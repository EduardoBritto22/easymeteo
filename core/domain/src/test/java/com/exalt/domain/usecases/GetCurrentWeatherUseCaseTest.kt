package com.exalt.domain.usecases

import com.exalt.domain.models.*
import com.exalt.domain.repositories.WeatherRemoteRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.joda.time.LocalDateTime
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetCurrentWeatherUseCaseTest {

    @Mock
    private lateinit var repository: WeatherRemoteRepository
    private lateinit var getCurrentWeatherUseCase: GetCurrentWeatherUseCase


    @Before
    fun setUp() {
        getCurrentWeatherUseCase = GetCurrentWeatherUseCase(repository)
    }

    @Test
    fun `Given remote repository returns a flow of weather data, When GetCurrentWeatherUseCase is invoked, Then returns the successful weather data flow`() =
        runTest {
            // Given a mocked remote repository injected into the use case..
            Mockito.`when`(repository.getCurrentWeatherBy(city)).thenReturn(flowOf(Result.success(weatherData)))

            // When the flow of weather data is returned from the use case...
            val actualWeatherData = getCurrentWeatherUseCase(city)

            // Then the result should be the weather data expected
            assertEquals(weatherData, actualWeatherData.first().getOrNull())
        }

    @Test
    fun `Given remote repository returns failure, When GetCurrentWeatherUseCase is invoked, Then returns the failed weather data flow`() =
        runTest {
            // Given a mocked remote repository injected into the use case..
            Mockito.`when`(repository.getCurrentWeatherBy(city)).thenReturn(flowOf(Result.failure(RuntimeException())))

            // When the flow of weather data is returned from the use case...
            val actualWeatherData = getCurrentWeatherUseCase(city)

            // Then the result should be a failure
            assertTrue(actualWeatherData.first().isFailure)
        }

    @Test
    fun `Given remote repository throws an exception, When GetCurrentWeatherUseCase is invoked, Then returns the failed weather data flow`() =
        runTest {
            // Given a mocked remote repository injected into the use case..
            Mockito.`when`(repository.getCurrentWeatherBy(city)).thenThrow(RuntimeException())

            // When the flow of weather data is returned from the use case...
            val actualWeatherData = getCurrentWeatherUseCase(city)

            // Then the result should be a failure
            assertTrue(actualWeatherData.first().isFailure)
        }

    private val city = City(
        id = 1,
        name = "Belo Horizonte",
        stateCode = "MG",
        countryCode = "BR",
        lat = null,
        lon = null,
        sunrise = null,
        sunset = null,
        timezone = null
    )

    private val weatherData = WeatherData(
        id = 1,
        time = LocalDateTime.now(),
        temperature = 50.0,
        feelsLike = 55.0,
        windSpeed = 20.0,
        humidity = 20,
        windDirectionDeg = 20,
        mainWeather = listOf(
            MainWeather(
                mainWeatherDescription = "description",
                mainWeatherGroup = "rain",
                mainWeatherIcon = "1d"
            )
        ),
    )

}