package com.exalt.domain.usecases

import com.exalt.domain.models.City
import com.exalt.domain.models.TimeStampWeatherData
import com.exalt.domain.repositories.WeatherRemoteRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.joda.time.LocalDateTime
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GetTimeStampsWeatherUseCaseTest {

    @Mock
    private lateinit var repository: WeatherRemoteRepository
    private lateinit var getTimeStampsWeatherUseCase: GetTimeStampsWeatherUseCase


    @Before
    fun setUp() {
        getTimeStampsWeatherUseCase = GetTimeStampsWeatherUseCase(repository)
    }

    @Test
    fun `Given remote repository returns a flow of TimeStampWeatherData list, When GetTimeStampsWeatherUseCase is invoked, Then returns the successful flow of TimeStampWeatherData list`() =
        runTest {
            // Given a mocked remote repository injected into the use case..
            Mockito.`when`(repository.getTimeStampWeatherBy(city,8)).thenReturn(flowOf(Result.success(timeStampWeatherData)))

            // When the flow of weather data is returned from the use case...
            val actualWeatherData = getTimeStampsWeatherUseCase(city, 8)

            // Then the result should be the weather data expected
            assertEquals(timeStampWeatherData, actualWeatherData.first().getOrNull())
        }

    @Test
    fun `Given remote repository returns failure, When GetTimeStampsWeatherUseCase is invoked, Then returns the failed flow of TimeStampWeatherData list`() =
        runTest {
            // Given a mocked remote repository injected into the use case..
            Mockito.`when`(repository.getTimeStampWeatherBy(city,8)).thenReturn(flowOf(Result.failure(RuntimeException())))

            // When the flow of TimeStampWeatherData list is returned from the use case...
            val actualWeatherData = getTimeStampsWeatherUseCase(city, 8)

            // Then the result should be a failure
            assertTrue(actualWeatherData.first().isFailure)
        }

    @Test
    fun `Given remote repository throws an exception, When GetTimeStampsWeatherUseCase is invoked, Then returns the failed flow of TimeStampWeatherData list`() =
        runTest {
            // Given a mocked remote repository injected into the use case..
            Mockito.`when`(repository.getTimeStampWeatherBy(city,8)).thenThrow(RuntimeException())

            // When the flow of TimeStampWeatherData list is returned from the use case...
            val actualWeatherData = getTimeStampsWeatherUseCase(city, 8)

            // Then the result should be a failure
            assertTrue(actualWeatherData.first().isFailure)
        }

    private val city = City(
        id = 1,
        name = "Belo Horizonte",
        stateCode = "MG",
        countryCode = "BR",
        lat = null,
        lon = null,
        sunrise = null,
        sunset = null,
        timezone = null
    )

    private val timeStampWeatherData = listOf(
        TimeStampWeatherData(
            precipitationProbability = 1.0,
            timeStamp = LocalDateTime.now(),
            temperature = 50.0,
            windSpeed = 20.0,
            mainWeatherIcon = "i1",
            humidity = 20,
            windDirectionDeg = 20
        ),
        TimeStampWeatherData(
            precipitationProbability = 0.0,
            timeStamp = LocalDateTime.now(),
            temperature = 50.0,
            windSpeed = 20.0,
            mainWeatherIcon = "i1",
            humidity = 20,
            windDirectionDeg = 20
        ),
        TimeStampWeatherData(
            precipitationProbability = 0.50,
            timeStamp = LocalDateTime.now(),
            temperature = 80.0,
            windSpeed = 50.0,
            mainWeatherIcon = "i1",
            humidity = 20,
            windDirectionDeg = 20
        ),
    )



}