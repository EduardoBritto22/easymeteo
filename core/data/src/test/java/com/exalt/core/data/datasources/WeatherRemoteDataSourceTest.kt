package com.exalt.core.data.datasources

import com.exalt.core.data.remote.api.WeatherApi
import com.exalt.core.data.remote.api.models.requests.CurrentWeatherRequest
import com.exalt.core.data.remote.api.models.requests.TimeStampWeatherRequest
import com.exalt.core.data.remote.WeatherRemoteDataSource
import com.exalt.core.data.remote.api.models.responses.*
import kotlinx.coroutines.test.runTest
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import retrofit2.Response

@RunWith(MockitoJUnitRunner::class)
class WeatherRemoteDataSourceTest {

    @Mock
    private lateinit var weatherApi: WeatherApi

    private lateinit var remoteDataSource: WeatherRemoteDataSource

    @Before
    fun setUp() {
        remoteDataSource = WeatherRemoteDataSource(weatherApi)
    }

    private val currentWeatherResponse = CurrentWeatherResponse(
        clouds = CloudsDto(all = 100),
        coord = CoordDto(0.0, 0.0),
        dt = 1000,
        id = 1,
        main = MainDto(feels_like = 20.0, grnd_level = 100, humidity = 30, pressure = 1000, sea_level = 10000, temp = 21.0),
        name = "Belo Horizonte",
        rain = RainDto(2.0, 2.0),
        wind = WindDto(deg = 0, gust = 0.0, speed = 100.0),
        snow = SnowDto(2.0, 2.0),
        visibility = 100,
        weather = emptyList(),
        sys = SysDto(country = "BR", sunset = 0, sunrise = 0),
        timezone = 1000
    )

    private val timeStampWeatherResponse = WeatherTimeStampsResponse(
        city = CityDto(
            CoordDto(0.0, 0.0),
            sunrise = 0,
            sunset = 0,
            country = "BR",
            id = 1,
            name = "Belo Horizonte",
            population = 2000000,
            timezone = 0
        ),
        list = listOf<TimeStampWeatherDto>()
    )

    @Test
    fun `Given weather api returns a success with a CurrentWeatherResponse, When getCurrentWeatherByCity is invoked, Then returns a CurrentWeatherResponse`() =
        runTest {

            val request = CurrentWeatherRequest(lat = null, lon = null, units = null, lang = null, q = "Belo Horizonte")
            val expected = Response.success(currentWeatherResponse)

            // Given a mocked weather api injected into the data source..
            Mockito.`when`(weatherApi.getCurrentWeatherByCity(request.toMap())).thenReturn(expected)

            // When the CurrentWeather response is returned...
            val weatherData = remoteDataSource.getCurrentWeatherBy(request)

            // Then the result should be a successful CurrentWeather response
            assertEquals(expected, weatherData)
        }

    @Test
    fun `Given weather api returns an error, When getCurrentWeatherByCity is invoked, Then returns an error response`() =
        runTest {

            val request = CurrentWeatherRequest(lat = null, lon = null, units = null, lang = null, q = "Belo Horizonte")
            val expected = Response.error<CurrentWeatherResponse>(404, ResponseBody.create(null, ""))

            // Given a mocked weather api injected into the data source..
            Mockito.`when`(weatherApi.getCurrentWeatherByCity(request.toMap())).thenReturn(expected)

            // When the CurrentWeather response is returned...
            val weatherData = remoteDataSource.getCurrentWeatherBy(request)

            // Then the result should be an error
            assertEquals(expected, weatherData)
        }


    @Test
    fun `Given weather api returns a success with a WeatherTimeStampsResponse, When getWeatherTimeStampsByCity is invoked, Then returns a TimeStampWeatherResponse`() =
        runTest {

            val request =
                TimeStampWeatherRequest(lat = null, lon = null, units = null, lang = null, q = "Belo Horizonte", cnt = 8)
            val expected = Response.success(timeStampWeatherResponse)

            // Given a mocked weather api injected into the data source..
            Mockito.`when`(weatherApi.getWeatherTimeStampsByCity(request.toMap())).thenReturn(expected)

            // When the TimeStampWeather response is returned...
            val weatherData = remoteDataSource.getWeatherTimeStampsBy(request)

            // Then the result should be a successful TimeStampWeather response
            assertEquals(expected, weatherData)
        }

    @Test
    fun `Given weather api returns an error, When getWeatherTimeStampsByCity is invoked, Then returns an error response`() =
        runTest {

            val request = TimeStampWeatherRequest(lat = null, lon = null, units = null, lang = null, q = "Belo Horizonte", cnt = 0)
            val expected = Response.error<WeatherTimeStampsResponse>(404, ResponseBody.create(null, ""))

            // Given a mocked weather api injected into the data source..
            Mockito.`when`(weatherApi.getWeatherTimeStampsByCity(request.toMap())).thenReturn(expected)

            // When the CurrentWeather response is returned...
            val weatherData = remoteDataSource.getWeatherTimeStampsBy(request)

            // Then the result should be an error
            assertEquals(expected, weatherData)
        }
}