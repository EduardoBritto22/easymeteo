package com.exalt.core.data.datasources

import com.exalt.core.data.local.CityLocalDataSource
import com.exalt.core.data.local.db.dao.CityDao
import com.exalt.core.data.local.db.entities.CityEntity
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CityLocalDataSourceTest {

    @Mock
    private lateinit var cityDao: CityDao

    private lateinit var localDataSource: CityLocalDataSource

    @Before
    fun setUp() {
        localDataSource = CityLocalDataSource(cityDao)
    }

    @Test
    fun `Given city db returns a list of cities, When getCityList is invoked, Then returns a list of cities`() =
        runTest {

            val expected = citiesList

            // Given a mocked City DAO injected into the data source..
            Mockito.`when`(cityDao.getAllCityEntities()).thenReturn(flowOf(citiesList))

            // When the list of cities is returned...
            val actualList = localDataSource.getCityList()

            // Then the result should be a full list of cities
            assertEquals(expected, actualList.first())
        }

    @Test
    fun `Given city db returns an empty list, When getCityList is invoked, Then returns an empty list`() =
        runTest {

            val expected = emptyList<CityEntity>()

            // Given a mocked City DAO injected into the data source..
            Mockito.`when`(cityDao.getAllCityEntities()).thenReturn(flowOf(emptyList()))

            // When the list of cities is returned...
            val actualList = localDataSource.getCityList()

            // Then the result should be a full list of cities
            assertEquals(expected, actualList.first())
        }


    @Test
    fun `Given city db save a new city, When saveCity is invoked, Then the list has the inserted city`() =
        runTest {

            val testCityList = mutableListOf<CityEntity>()
            val newCity = citiesList.first()

            // Given a mocked City DAO injected into the data source..
            Mockito.`when`(cityDao.insert(newCity)).then {
                testCityList.add(newCity)
            }

            // When we save a new city..
            localDataSource.saveCity(newCity)

            // Then the list of cities should not be empty and have the new city
            assertTrue(testCityList.isNotEmpty())
            assertEquals(newCity, testCityList.first())
        }

    @Test
    fun `Given city db delete a city, When remove is invoked, Then the list has the removed the city`() =
        runTest {

            val testCityList = mutableListOf<CityEntity>()
            testCityList.addAll(citiesList)

            val deletedCity = citiesList.first()

            // Given a mocked City DAO injected into the data source..
            Mockito.`when`(cityDao.delete(deletedCity)).then {
                testCityList.remove(deletedCity)
            }

            // When we remove a city..
            localDataSource.remove(deletedCity)

            // Then the list of cities should not have the removed city
            assertFalse(testCityList.contains(deletedCity))
        }


    private val citiesList = listOf(
        CityEntity(
            id = 0,
            name = "Belo Horizonte",
            countryCode = "BR",
            stateCode = "MG",
            latitude = null,
            longitude = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        CityEntity(
            id = 1,
            name = "Paris",
            countryCode = "FR",
            stateCode = null,
            latitude = null,
            longitude = null,
            sunrise = null,
            sunset = null,
            timezone = null
        ),
        CityEntity(
            id = 2,
            name = "Contagem",
            countryCode = "BR",
            stateCode = "MG",
            latitude = null,
            longitude = null,
            sunrise = null,
            sunset = null,
            timezone = null
        )
    )

}