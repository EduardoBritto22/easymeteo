package com.exalt.core.data.mappers

import com.exalt.core.data.remote.api.models.responses.*
import com.exalt.domain.models.WeatherData
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.junit.Assert.assertEquals
import org.junit.Test

class WeatherDataMapperTest{

    private var weatherDataMapper : WeatherDataMapper = WeatherDataMapper(MainWeatherMapper())

    @Test
    fun `Given a CurrentWeatherResponse, When mapper is called, Then returns the right Weather data model`() {

        // 3/12/2022 6h20:00
        val dateTime = LocalDateTime(2022,12,3,6,20,0)

        // Given
        val currentWeatherResponse = CurrentWeatherResponse(
            clouds = CloudsDto(all = 100),
            coord = CoordDto(0.0, 0.0),
            dt = dateTime.toDateTime(DateTimeZone.UTC).millis.div(1000),
            id = 1,
            main = MainDto(feels_like = 55.0, grnd_level = 100, humidity = 20, pressure = 1000, sea_level = 10000, temp = 50.0),
            name = "Belo Horizonte",
            rain = RainDto(2.0, 2.0),
            wind = WindDto(deg = 0, gust = 0.0, speed = 20.0),
            snow = SnowDto(2.0, 2.0),
            visibility = 100,
            weather = emptyList(),
            sys = SysDto(country = "BR", sunset = 0, sunrise = 0),
            timezone = 1000
        )
        val expectedWeatherDataModel =  WeatherData(
            id = 1,
            time = dateTime,
            temperature = 50.0,
            feelsLike = 55.0,
            windSpeed = 20.0,
            humidity = 20,
            windDirectionDeg = 0,
            mainWeather = emptyList(),
        )

        // When
        val actualWeatherDataModel = weatherDataMapper.toWeatherData(currentWeatherResponse)

        // Then
        assertEquals(expectedWeatherDataModel, actualWeatherDataModel)
    }

}