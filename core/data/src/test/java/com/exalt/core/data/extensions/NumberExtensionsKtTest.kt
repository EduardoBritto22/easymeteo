package com.exalt.core.data.extensions

import junit.framework.TestCase
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.junit.Test

class NumberExtensionsKtTest {


    @Test
    fun `Given an offset of 3600 seconds , When call the convert function, Then return the correct timezone UTC + 1h` () {

        val expectedTimeZone = DateTimeZone.forOffsetHours(1)

        //Given
        val unixTimeZoneOffset = 3600 // seconds in 1h

        //When
        val timeZone = unixTimeZoneOffset.fromUnixTimeToDateTimeZone()

        //Then
        TestCase.assertEquals(expectedTimeZone,timeZone)
    }

    @Test
    fun `Given an offset of -3600 seconds , When call the convert function, Then return the correct timezone UTC - 1h` () {

        val expectedTimeZone = DateTimeZone.forOffsetHours(-1)

        //Given
        val unixTimeZoneOffset: Int = -3600 // seconds in 1h

        //When
        val timeZone = unixTimeZoneOffset.fromUnixTimeToDateTimeZone()

        //Then
        TestCase.assertEquals(expectedTimeZone,timeZone)
    }

    @Test
    fun `Given an offset of 0 seconds , When call the convert function, Then return the correct timezone UTC` () {

        val expectedTimeZone = DateTimeZone.UTC

        //Given
        val unixTimeZoneOffset = 0

        //When
        val timeZone = unixTimeZoneOffset.fromUnixTimeToDateTimeZone()

        //Then
        TestCase.assertEquals(expectedTimeZone,timeZone)
    }

    @Test
    fun `Given a date time of 28-02-2023 06H36,20 , When call the convert function, Then return the correct local date time` () {

        val expectedLocalDateTime = LocalDateTime(2023,2,28,6,36,20)

        //Given
        val unixDateTime: Long = 1677566180

        //When
        val timeZone = unixDateTime.fromUnixTimeToLocalDateTime()

        //Then
        TestCase.assertEquals(expectedLocalDateTime,timeZone)
    }



}