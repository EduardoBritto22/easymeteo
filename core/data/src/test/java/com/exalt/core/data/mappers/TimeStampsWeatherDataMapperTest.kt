package com.exalt.core.data.mappers

import com.exalt.core.data.remote.api.models.responses.*
import com.exalt.domain.models.TimeStampWeatherData
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.junit.Assert.assertEquals
import org.junit.Test

class TimeStampsWeatherDataMapperTest {

    private var timeStampsWeatherDataMapper: TimeStampsWeatherDataMapper = TimeStampsWeatherDataMapper()

    @Test
    fun `Given a WeatherTimeStampsResponse, When mapper is called, Then returns the right list of TimeStampsWeatherData model`() {

        // 3/12/2022 6h20:00
        val dateTime = LocalDateTime(2022, 12, 3, 6, 20, 0)

        // Given
        val weatherTimeStampsResponse = WeatherTimeStampsResponse(
            city = CityDto(
                CoordDto(0.0, 0.0),
                sunrise = 0,
                sunset = 0,
                country = "BR",
                id = 1,
                name = "Belo Horizonte",
                population = 2000000,
                timezone = 0
            ),
            list = listOf(
                TimeStampWeatherDto(
                    clouds = CloudsDto(all = 100),
                    dt = dateTime.toDateTime(DateTimeZone.UTC).millis.div(1000),
                    main = MainDto(feels_like = 55.0, grnd_level = 100, humidity = 20, pressure = 1000, sea_level = 10000, temp = 50.0),
                    rain = RainDto(2.0, 2.0),
                    wind = WindDto(deg = 0, gust = 0.0, speed = 20.0),
                    snow = SnowDto(2.0, 2.0),
                    visibility = 100,
                    weather = emptyList(),
                    pop = 0.0,
                    dt_txt = dateTime.toDateTime(DateTimeZone.UTC).toString()
                ),
                TimeStampWeatherDto(
                    clouds = CloudsDto(all = 100),
                    dt = dateTime.toDateTime(DateTimeZone.UTC).millis.div(1000),
                    main = MainDto(feels_like = 55.0, grnd_level = 100, humidity = 20, pressure = 1000, sea_level = 10000, temp = 50.0),
                    rain = RainDto(2.0, 2.0),
                    wind = WindDto(deg = 0, gust = 0.0, speed = 20.0),
                    snow = SnowDto(2.0, 2.0),
                    visibility = 100,
                    weather = emptyList(),
                    pop = 50.0,
                    dt_txt = dateTime.toDateTime(DateTimeZone.UTC).toString()
                )
            )
        )
        val expectedWeatherDataModel = listOf(
            TimeStampWeatherData(
                timeStamp = dateTime,
                temperature = 50.0,
                windSpeed = 20.0,
                humidity = 20,
                windDirectionDeg = 0,
                mainWeatherIcon = "",
                precipitationProbability = 0.0
            ),
            TimeStampWeatherData(
                timeStamp = dateTime,
                temperature = 50.0,
                windSpeed = 20.0,
                humidity = 20,
                windDirectionDeg = 0,
                mainWeatherIcon = "",
                precipitationProbability = 50.0
            )
        )

        // When
        val actualTimeStampsWeatherDataListModel = timeStampsWeatherDataMapper.toTimeStampsWeatherDataList(weatherTimeStampsResponse)

        // Then
        assertEquals(expectedWeatherDataModel, actualTimeStampsWeatherDataListModel)
    }

}