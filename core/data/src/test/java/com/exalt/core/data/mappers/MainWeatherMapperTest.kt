package com.exalt.core.data.mappers

import com.exalt.core.data.remote.api.models.responses.WeatherDto
import com.exalt.domain.models.MainWeather
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class MainWeatherMapperTest {

    private var mainWeatherMapper: MainWeatherMapper = MainWeatherMapper()

    @Test
    fun `Given list of weather DTOs, When mapper is called, Then returns list of main weather models`() {

        // Given
        val expectedMainWeatherModels = mainWeatherList

        // When
        val actualMainWeatherModels = mainWeatherMapper.toMainWeatherList(mainWeatherDtoList)

        // Then
        assertEquals(expectedMainWeatherModels, actualMainWeatherModels)
    }

    @Test
    fun `Given empty list of weather DTOs, When mapper is called, Then returns an empty list`() {

        // Given
        val dtoList = emptyList<WeatherDto>()
        // When
        val actualMainWeatherModels = mainWeatherMapper.toMainWeatherList(dtoList)

        // Then
        assertTrue(actualMainWeatherModels.isEmpty())
    }

    private val mainWeatherDtoList = listOf(
        WeatherDto(
            description = "test",
            icon = "1d",
            id = 0,
            main = "main"
        ),
        WeatherDto(
            description = "test 2",
            icon = "2d",
            id = 1,
            main = "main 2"
        )
    )

    private val mainWeatherList = listOf(
        MainWeather(
            mainWeatherDescription = "test",
            mainWeatherGroup = "main",
            mainWeatherIcon = "1d",
        ),
        MainWeather(
            mainWeatherDescription = "test 2",
            mainWeatherGroup = "main 2",
            mainWeatherIcon = "2d",
        )
    )


}