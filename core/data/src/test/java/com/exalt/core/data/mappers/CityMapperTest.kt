package com.exalt.core.data.mappers

import com.exalt.core.data.local.db.entities.CityEntity
import com.exalt.core.data.remote.api.models.responses.CityDto
import com.exalt.core.data.remote.api.models.responses.CoordDto
import com.exalt.domain.models.City
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.junit.Assert.assertEquals
import org.junit.Test

class CityMapperTest {

    private var cityMapper : CityMapper = CityMapper()

    @Test
    fun `Given a City DTO, When mapper is called, Then returns the right model`() {

        // 3/12/2022 6h20:00
        val sunrise = LocalDateTime(2022,12,3,6,20,0)
        // 3/12/2022 18h30:00
        val sunset =  LocalDateTime(2022,12,3,18,30,0)

        // Given
        val cityDto = CityDto(
            CoordDto(100.0, -100.0),
            sunrise = sunrise.toDateTime(DateTimeZone.UTC).millis.div(1000),
            sunset = sunset.toDateTime(DateTimeZone.UTC).millis.div(1000),
            country = "BR",
            id = 0,
            name = "Belo Horizonte",
            population = 2000000,
            timezone = 3600
        )

        val expectedCityModel = City(
            id = 0,
            lat = 100.0,
            lon = -100.0,
            countryCode = "BR",
            name = "Belo Horizonte",
            timezone = DateTimeZone.forOffsetHours(1),
            stateCode = "",
            sunrise = sunrise,
            sunset = sunset
        )

        // When
        val actualCityModel = cityMapper.toModel(cityDto)

        // Then
        assertEquals(expectedCityModel, actualCityModel)
    }

    @Test
    fun `Given a City Entity, When mapper is called, Then returns the right model`() {

        // 3/12/2022 6h20:00
        val sunrise = LocalDateTime(2022,12,3,6,20,0)
        // 3/12/2022 18h30:00
        val sunset =  LocalDateTime(2022,12,3,18,30,0)

        // Given
        val cityEntity = CityEntity(
            latitude = 100.0,
            longitude = -100.0,
            sunrise = sunrise,
            sunset = sunset,
            countryCode = "BR",
            id = 0,
            name = "Belo Horizonte",
            timezone = DateTimeZone.forOffsetHours(1),
            stateCode = ""
        )

        val expectedCityModel = City(
            id = 0,
            lat = 100.0,
            lon = -100.0,
            countryCode = "BR",
            name = "Belo Horizonte",
            timezone = DateTimeZone.forOffsetHours(1),
            stateCode = "",
            sunrise = sunrise,
            sunset = sunset
        )

        // When
        val actualCityModel = cityMapper.toModel(cityEntity)

        // Then
        assertEquals(expectedCityModel, actualCityModel)
    }

    @Test
    fun `Given a City Model, When mapper is called, Then returns the right entity`() {

        // 3/12/2022 6h20:00
        val sunrise = LocalDateTime(2022,12,3,6,20,0)
        // 3/12/2022 18h30:00
        val sunset =  LocalDateTime(2022,12,3,18,30,0)

        // Given
        val expectedCityEntity = CityEntity(
            latitude = 100.0,
            longitude = -100.0,
            sunrise = sunrise,
            sunset = sunset,
            countryCode = "BR",
            id = 0,
            name = "Belo Horizonte",
            timezone = DateTimeZone.forOffsetHours(1),
            stateCode = ""
        )

        val cityModel = City(
            id = 0,
            lat = 100.0,
            lon = -100.0,
            countryCode = "BR",
            name = "Belo Horizonte",
            timezone = DateTimeZone.forOffsetHours(1),
            stateCode = "",
            sunrise = sunrise,
            sunset = sunset
        )

        // When
        val actualCityEntity = cityMapper.toEntity(cityModel)

        // Then
        assertEquals(expectedCityEntity, actualCityEntity)
    }

}