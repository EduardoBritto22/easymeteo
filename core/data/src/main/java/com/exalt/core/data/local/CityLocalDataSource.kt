package com.exalt.core.data.local

import com.exalt.core.data.local.db.dao.CityDao
import com.exalt.core.data.local.db.entities.CityEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CityLocalDataSource @Inject constructor(
    private val cityDao: CityDao
){

    fun getCityList(): Flow<List<CityEntity>> = cityDao.getAllCityEntities()


    suspend fun saveCity(city: CityEntity){
        cityDao.insert(city)
    }

    suspend fun remove(city: CityEntity) {
        cityDao.delete(city)
    }
}