package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the rain volume.
 *
 * @property 1h  Rain volume for last 1 hour, mm.
 * @property 3h  Rain volume for last 3 hour, mm.
 */
data class RainDto(
    val `1h`: Double?,
    val `3h`: Double?
)