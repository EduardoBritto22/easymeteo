package com.exalt.core.data.di

import android.content.Context
import androidx.room.Room
import com.exalt.core.data.local.db.CityDatabase
import com.exalt.core.data.local.db.dao.CityDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataBaseModule {

    @Provides
    @Singleton
    fun providesCityDatabase(
        @ApplicationContext context: Context
    ): CityDatabase = Room.databaseBuilder(
        context,
        CityDatabase::class.java,
        "city-database"
    ).build()

    @Provides
    fun providesCityDao(
        database: CityDatabase
    ): CityDao = database.cityDao()

}