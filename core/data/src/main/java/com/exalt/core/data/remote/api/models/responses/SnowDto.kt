package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the snow volume.
 *
 * @property 1h  Snow volume for last 1 hour, mm.
 * @property 3h  Snow volume for last 3 hour, mm.
 */
data class SnowDto(
    val `1h`: Double,
    val `3h`: Double
)