package com.exalt.core.data.remote.api.models.responses

/**
 * Represents  weather condition.
 * @property description Weather condition within the group.
 * @property icon Weather icon id.
 * @property id  Weather condition id.
 * @property main Group of weather parameters (Rain, Snow, Extreme etc.).
 */
data class WeatherDto(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
)