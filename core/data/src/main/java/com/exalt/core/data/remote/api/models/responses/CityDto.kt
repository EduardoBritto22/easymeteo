package com.exalt.core.data.remote.api.models.responses

/**
 * Represents a city for the weather data.
 * @property coord City geo location.
 * @property country Country code (GB, JP etc.).
 * @property id City ID.
 * @property name City name.
 * @property population City population.
 * @property sunrise Sunrise time, Unix, UTC.
 * @property sunset  Sunset time, Unix, UTC.
 * @property timezone Shift in seconds from UTC.
 */
data class CityDto(
    val coord: CoordDto,
    val country: String,
    val id: Int,
    val name: String,
    val population: Long,
    val sunrise: Long,
    val sunset: Long,
    val timezone: Int
)