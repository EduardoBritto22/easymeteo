package com.exalt.core.data.mappers

import com.exalt.core.data.extensions.fromUnixTimeToDateTimeZone
import com.exalt.core.data.extensions.fromUnixTimeToLocalDateTime
import com.exalt.core.data.local.db.entities.CityEntity
import com.exalt.core.data.remote.api.models.requests.CurrentWeatherRequest
import com.exalt.core.data.remote.api.models.requests.TimeStampWeatherRequest
import com.exalt.core.data.remote.api.models.responses.CityDto
import com.exalt.domain.models.City
import javax.inject.Inject

class CityMapper @Inject constructor() {

    fun toModel(cityEntity: CityEntity): City =
        City(
            id = cityEntity.id,
            name = cityEntity.name,
            stateCode = cityEntity.stateCode,
            countryCode = cityEntity.countryCode,
            lat = cityEntity.latitude,
            lon = cityEntity.longitude,
            timezone = cityEntity.timezone,
            sunset = cityEntity.sunset,
            sunrise = cityEntity.sunrise
        )


    fun toEntity(city: City): CityEntity =
        CityEntity(
            id = city.id,
            name = city.name,
            stateCode = city.stateCode,
            countryCode = city.countryCode,
            latitude = city.lat,
            longitude = city.lon,
            sunset = city.sunset,
            sunrise = city.sunrise,
            timezone = city.timezone
        )


    fun toModel(cityDto: CityDto): City =
        City(
            id = cityDto.id,
            name = cityDto.name,
            stateCode = "",
            countryCode = cityDto.country,
            lat = cityDto.coord.lat,
            lon = cityDto.coord.lon,
            timezone = cityDto.timezone.fromUnixTimeToDateTimeZone(),
            sunset = cityDto.sunset.fromUnixTimeToLocalDateTime(),
            sunrise = cityDto.sunrise.fromUnixTimeToLocalDateTime()
        )

    fun toCurrentWeatherRequest(city: City): CurrentWeatherRequest =
        CurrentWeatherRequest(
            lat = city.lat,
            lon = city.lon,
            q = "${city.name}, ${city.stateCode.orEmpty()},${city.countryCode}"
        )

    fun toTimeStampsWeatherRequest(city: City): TimeStampWeatherRequest =
        TimeStampWeatherRequest(
            lat = city.lat,
            lon = city.lon,
            q = "${city.name}, ${city.stateCode.orEmpty()},${city.countryCode}"
        )
}