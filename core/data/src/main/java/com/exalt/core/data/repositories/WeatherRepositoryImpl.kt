package com.exalt.core.data.repositories

import com.exalt.core.data.mappers.CityMapper
import com.exalt.core.data.mappers.TimeStampsWeatherDataMapper
import com.exalt.core.data.mappers.WeatherDataMapper
import com.exalt.core.data.remote.WeatherRemoteDataSource
import com.exalt.domain.models.City
import com.exalt.domain.models.TimeStampWeatherData
import com.exalt.domain.models.WeatherData
import com.exalt.domain.repositories.WeatherRemoteRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepositoryImpl @Inject constructor(
    private val cityMapper: CityMapper,
    private val weatherDataMapper: WeatherDataMapper,
    private val timeStampsWeatherDataMapper: TimeStampsWeatherDataMapper,
    private val weatherRemoteDataSource: WeatherRemoteDataSource
): WeatherRemoteRepository {

    override fun getCurrentWeatherBy(city: City): Flow<Result<WeatherData>> = flow {

        val response = weatherRemoteDataSource.getCurrentWeatherBy(cityMapper.toCurrentWeatherRequest(city))
        if(response.isSuccessful && response.body() != null){
            emit(Result.success(weatherDataMapper.toWeatherData(response.body()!!)))
        } else {
            emit(Result.failure(Exception(response.message())))
        }
    }

    override fun getTimeStampWeatherBy(city: City, timeStampCount: Int): Flow<Result<List<TimeStampWeatherData>>> = flow {

        val response = weatherRemoteDataSource.getWeatherTimeStampsBy(cityMapper.toTimeStampsWeatherRequest(city))

        if(response.isSuccessful && response.body() != null){
            emit(Result.success(timeStampsWeatherDataMapper.toTimeStampsWeatherDataList(response.body()!!)))
        } else {
            emit(Result.failure(Exception(response.message())))
        }
    }


}