package com.exalt.core.data.remote.api.models.requests

/**
 * Represents a request to get the current weather .
 * @property lat Geographical coordinate latitude.
 * @property lon Geographical coordinate longitude.
 * @property units Units of measurement. 'standard', 'metric' and 'imperial' units are available.
 * @property lang The output language.
 * @property q  String with city name, state code and country code divided by comma.
 */
data class CurrentWeatherRequest(
    val lat: Double?,
    val lon: Double?,
    var units: String? = "metric",
    var lang: String? = "en",
    val q: String?
) {
    fun toMap(): Map<String, String> {

        val map = mutableMapOf<String,String>()
        lat?.let {
            map["lat"] = it.toString()
        }
        lon?.let {
            map["lon"] = it.toString()
        }
        units?.let {
            map["units"] = it
        }
        lang?.let {
            map["lang"] = it
        }
        q?.let {
            map["q"] = it
        }

        return map
    }
}
