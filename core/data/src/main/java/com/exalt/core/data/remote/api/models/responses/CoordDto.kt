package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the City geo location.
 * @property lat City geo location, longitude.
 * @property lon City geo location, latitude.
 */
data class CoordDto(
    val lat: Double,
    val lon: Double
)