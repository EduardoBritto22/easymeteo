package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the weather data for a specific time stamp.
 *
 * @property clouds Cloudiness.
 * @property dt Time of data calculation, unix, UTC.
 * @property dt_text  Time of data forecasted, ISO, UTC.
 * @property main Main weather data.
 * @property pop Probability of precipitation. The values of the parameter vary between 0 and 1, where 0 is equal to 0%, 1 is equal to 100%.
 * @property rain Rain volume.
 * @property snow Snow volume.
 * @property visibility Visibility, meter. The maximum value of the visibility is 10km.
 * @property weather List of Weather conditions.
 * @property wind  Wind condition .
 *
 */
data class TimeStampWeatherDto(
    val clouds: CloudsDto,
    val dt: Long,
    val dt_txt: String,
    val main: MainDto,
    val pop: Double,
    val rain: RainDto,
    val snow: SnowDto,
    val visibility: Int,
    val weather: List<WeatherDto>,
    val wind: WindDto
)