package com.exalt.core.data.mappers

import com.exalt.core.data.extensions.fromUnixTimeToLocalDateTime
import com.exalt.core.data.remote.api.models.responses.CurrentWeatherResponse
import com.exalt.domain.models.WeatherData
import javax.inject.Inject

class WeatherDataMapper @Inject constructor(
    private val mainWeatherMapper: MainWeatherMapper,
) {

    fun toWeatherData(currentWeatherResponse: CurrentWeatherResponse) =
        WeatherData(
            id = currentWeatherResponse.id,
            temperature = currentWeatherResponse.main.temp,
            feelsLike = currentWeatherResponse.main.feels_like,
            humidity = currentWeatherResponse.main.humidity,
            windDirectionDeg = currentWeatherResponse.wind.deg,
            windSpeed = currentWeatherResponse.wind.speed,
            time = currentWeatherResponse.dt.fromUnixTimeToLocalDateTime(),
            mainWeather = mainWeatherMapper.toMainWeatherList(currentWeatherResponse.weather),
        )
}