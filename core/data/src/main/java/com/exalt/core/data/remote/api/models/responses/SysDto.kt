package com.exalt.core.data.remote.api.models.responses

/**
 * Represents a city extra data.
 * @property country Country code (GB, JP etc.).
 * @property sunrise Sunrise time, Unix, UTC.
 * @property sunset  Sunset time, Unix, UTC.
 */
data class SysDto(
    val country: String,
    val sunrise: Int,
    val sunset: Int,
)