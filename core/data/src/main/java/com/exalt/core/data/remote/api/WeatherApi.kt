package com.exalt.core.data.remote.api

import com.exalt.core.data.remote.api.models.responses.CurrentWeatherResponse
import com.exalt.core.data.remote.api.models.responses.WeatherTimeStampsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface WeatherApi {

    @GET("weather")
    suspend fun getCurrentWeatherByCity(
        @QueryMap currentWeatherRequest: Map<String, String>
    ): Response<CurrentWeatherResponse>

    @GET("forecast")
    suspend fun getWeatherTimeStampsByCity(
        @QueryMap timestampsWeatherRequest: Map<String, String>
    ): Response<WeatherTimeStampsResponse>
}