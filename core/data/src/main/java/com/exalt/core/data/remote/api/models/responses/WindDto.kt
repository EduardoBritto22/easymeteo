package com.exalt.core.data.remote.api.models.responses
/**
 * Represents the wind condition.
 *
 * @property deg  Wind direction, degrees (meteorological).
 * @property gust Wind gust. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour
 * @property speed Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
 */
data class WindDto(
    val deg: Int,
    val gust: Double,
    val speed: Double
)