package com.exalt.core.data.remote.api.models.requests

/**
 * Represents a request to get the weather in time stamps.
 * @property lat Geographical coordinate latitude.
 * @property lon Geographical coordinate longitude.
 * @property units Units of measurement. 'standard', 'metric' and 'imperial' units are available.
 * @property lang The output language.
 * @property cnt A number of timestamps, which will be returned in the API response.
 * @property q  String with city name, state code and country code divided by comma.
 */
data class TimeStampWeatherRequest(
    val lat: Double?,
    val lon: Double?,
    var units: String? = "metric",
    var lang: String? = "en",
    var cnt: Int = 8,
    val q: String?
) {
    fun toMap(): Map<String, String> {

        val map = mutableMapOf<String,String>()
        lat?.let {
            map["lat"] = it.toString()
        }
        lon?.let {
            map["lon"] = it.toString()
        }
        units?.let {
            map["units"] = it
        }
        lang?.let {
            map["lang"] = it
        }
        q?.let {
            map["q"] = it
        }

        map["cnt"] = cnt.toString()

        return map
    }
}
