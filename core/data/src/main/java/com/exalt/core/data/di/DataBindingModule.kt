package com.exalt.core.data.di

import com.exalt.core.data.repositories.*
import com.exalt.domain.repositories.CityLocalRepository
import com.exalt.domain.repositories.WeatherRemoteRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class DataBindingModule {

    @Binds
    internal abstract fun bindWeatherRemoteRepository(impl: WeatherRepositoryImpl): WeatherRemoteRepository

    @Binds
    internal abstract fun bindCityLocalRepository(impl: CityLocalRepositoryImpl): CityLocalRepository

}