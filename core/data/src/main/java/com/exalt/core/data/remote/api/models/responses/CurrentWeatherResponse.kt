package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the response to a current weather request for a city.
 * @property clouds Cloudiness.
 * @property coord City geo location.
 * @property dt Time of data calculation, unix, UTC.
 * @property id City ID.
 * @property main Main weather data.
 * @property name City name.
 * @property rain Rain volume.
 * @property snow Snow volume.
 * @property sys Other city information.
 * @property timezone  Shift in seconds from UTC.
 * @property visibility Visibility, meter. The maximum value of the visibility is 10km.
 * @property weather  Weather condition.
 * @property wind  Wind condition .
 *
 */
data class CurrentWeatherResponse(
    val clouds: CloudsDto,
    val coord: CoordDto,
    val dt: Long,
    val id: Int,
    val main: MainDto,
    val name: String,
    val rain: RainDto?,
    val snow: SnowDto?,
    val sys: SysDto?,
    val timezone: Int,
    val visibility: Int,
    val weather: List<WeatherDto>,
    val wind: WindDto
)