package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the clouds condition .
 * @property all Cloudiness, %.
 */
data class CloudsDto(
    val all: Int
)