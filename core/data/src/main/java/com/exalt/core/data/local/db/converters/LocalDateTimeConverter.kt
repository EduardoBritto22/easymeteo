package com.exalt.core.data.local.db.converters

import androidx.room.TypeConverter
import org.joda.time.LocalDateTime

class LocalDateTimeConverter {

    @TypeConverter
    fun localDateTimeToString(dateTime: LocalDateTime?): String? {
        dateTime?.let {
            return dateTime.toString()
        }
        return null
    }

    @TypeConverter
    fun fromString(dateTime: String?): LocalDateTime? {
        dateTime?.let {
            return LocalDateTime(it)
        }
        return null
    }

}