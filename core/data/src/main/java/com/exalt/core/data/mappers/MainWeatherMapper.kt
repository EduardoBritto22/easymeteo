package com.exalt.core.data.mappers

import com.exalt.core.data.remote.api.models.responses.WeatherDto
import com.exalt.domain.models.MainWeather
import javax.inject.Inject

class MainWeatherMapper @Inject constructor() {

    fun toMainWeatherList(weatherList: List<WeatherDto>): List<MainWeather> =
        weatherList.map { toMainWeather(it) }

    private fun toMainWeather(weatherDto: WeatherDto): MainWeather =
        MainWeather(
            mainWeatherIcon = weatherDto.icon,
            mainWeatherGroup = weatherDto.main,
            mainWeatherDescription = weatherDto.description
        )

}