package com.exalt.core.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.exalt.core.data.local.db.converters.DateTimeZoneConverter
import com.exalt.core.data.local.db.converters.LocalDateTimeConverter
import com.exalt.core.data.local.db.dao.CityDao
import com.exalt.core.data.local.db.entities.CityEntity

@Database(entities = [CityEntity::class], version = 1)
@TypeConverters(DateTimeZoneConverter::class, LocalDateTimeConverter::class)
abstract class CityDatabase: RoomDatabase() {
    abstract fun cityDao(): CityDao
}