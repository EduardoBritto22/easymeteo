package com.exalt.core.data.local.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.exalt.core.data.local.db.entities.CityEntity
import kotlinx.coroutines.flow.Flow

/**
 * DAO for [CityEntity] access
 */
@Dao
interface CityDao {

    /**
     * Inserts [city] into the db if it doesn't exist, and ignores it does
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(city: CityEntity)

    @Query(
        value = """
        SELECT * FROM cities
    """,
    )
    fun getAllCityEntities(): Flow<List<CityEntity>>

    @Delete
    suspend fun delete(city: CityEntity)

}