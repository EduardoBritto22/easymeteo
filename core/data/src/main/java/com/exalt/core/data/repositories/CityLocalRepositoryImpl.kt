package com.exalt.core.data.repositories

import com.exalt.core.data.local.CityLocalDataSource
import com.exalt.core.data.mappers.CityMapper
import com.exalt.domain.models.City
import com.exalt.domain.repositories.CityLocalRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CityLocalRepositoryImpl @Inject constructor(
    private val cityLocalDataSource: CityLocalDataSource,
    private val cityMapper: CityMapper
): CityLocalRepository {

    override fun getCitiesList(): Flow<List<City>> {
        return cityLocalDataSource.getCityList().map { cityList->
            cityList.map { cityMapper.toModel(it) }
        }
    }

    override suspend fun save(city: City) {
        cityLocalDataSource.saveCity(cityMapper.toEntity(city))
    }

    override suspend fun remove(city: City) {
        cityLocalDataSource.remove(cityMapper.toEntity(city))
    }
}