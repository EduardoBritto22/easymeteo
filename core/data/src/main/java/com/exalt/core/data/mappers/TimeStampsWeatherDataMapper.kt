package com.exalt.core.data.mappers

import com.exalt.core.data.extensions.fromUnixTimeToLocalDateTime
import com.exalt.core.data.remote.api.models.responses.TimeStampWeatherDto
import com.exalt.core.data.remote.api.models.responses.WeatherTimeStampsResponse
import com.exalt.domain.models.TimeStampWeatherData
import javax.inject.Inject

class TimeStampsWeatherDataMapper @Inject constructor() {


    fun toTimeStampsWeatherDataList(body: WeatherTimeStampsResponse): List<TimeStampWeatherData> =
        body.list.map { toTimeStampsWeatherData(it) }


    private fun toTimeStampsWeatherData(timeStampWeatherDto: TimeStampWeatherDto): TimeStampWeatherData {
        return TimeStampWeatherData(
            temperature = timeStampWeatherDto.main.temp,
            humidity = timeStampWeatherDto.main.humidity,
            windDirectionDeg = timeStampWeatherDto.wind.deg,
            windSpeed = timeStampWeatherDto.wind.speed,
            timeStamp = timeStampWeatherDto.dt.fromUnixTimeToLocalDateTime(),
            precipitationProbability = timeStampWeatherDto.pop,
            mainWeatherIcon = timeStampWeatherDto.weather.firstOrNull()?.icon.orEmpty()
        )
    }
}