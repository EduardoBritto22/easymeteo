package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the main weather data.
 *
 * @property feels_like  This temperature parameter accounts for the human perception of weather.
 * @property grnd_level Atmospheric pressure on the ground level, hPa.
 * @property humidity  Humidity, %.
 * @property pressure Atmospheric pressure on the sea level by default, hPa.
 * @property sea_level  Atmospheric pressure on the sea level, hPa.
 * @property temp Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
 *
 */
data class MainDto(
    val feels_like: Double,
    val grnd_level: Int,
    val humidity: Int,
    val pressure: Int,
    val sea_level: Int,
    val temp: Double
)