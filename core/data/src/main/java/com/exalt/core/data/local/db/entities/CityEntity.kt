package com.exalt.core.data.local.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime

/**
 * Defines a city a user wants to see the weather.
 */
@Entity( tableName = "cities",)
data class CityEntity(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "city_name") val name: String,
    @ColumnInfo(name = "country_code") val countryCode: String,
    @ColumnInfo(name = "state_code") val stateCode: String?,
    @ColumnInfo(name = "coord_lat") val latitude: Double?,
    @ColumnInfo(name = "coord_lon") val longitude: Double?,
    @ColumnInfo(name = "sunrise") val sunrise: LocalDateTime?,
    @ColumnInfo(name = "sunset") val sunset: LocalDateTime?,
    @ColumnInfo(name = "city_timezone") val timezone: DateTimeZone?
)