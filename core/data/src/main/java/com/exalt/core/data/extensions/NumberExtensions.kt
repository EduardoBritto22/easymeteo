package com.exalt.core.data.extensions

import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime

fun Int.fromUnixTimeToDateTimeZone(): DateTimeZone {
    return DateTimeZone.forOffsetMillis(this*1000)
}

fun Long.fromUnixTimeToLocalDateTime(): LocalDateTime {
    return DateTime(this*1000, DateTimeZone.UTC).toLocalDateTime()
}