package com.exalt.core.data.local.db.converters

import androidx.room.TypeConverter
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

class DateTimeZoneConverter {
    @TypeConverter
    fun dateTimeZoneToInt(timeZone: DateTimeZone?): Int? {
        timeZone?.let {
            return timeZone.getOffset(DateTime())
        }
        return null
    }

    @TypeConverter
    fun fromInt(unixTimeZone: Int?): DateTimeZone? {
        unixTimeZone?.let {
            return DateTimeZone.forOffsetMillis(it)
        }
        return null
    }
}