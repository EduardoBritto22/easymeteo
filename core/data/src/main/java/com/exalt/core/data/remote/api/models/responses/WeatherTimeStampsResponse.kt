package com.exalt.core.data.remote.api.models.responses

/**
 * Represents the response to a weather in time stamps request.
 * @property city The city for the weather time stamps.
 * @property list List of weather time stamps data.
 */
data class WeatherTimeStampsResponse(
    val city: CityDto,
    val list: List<TimeStampWeatherDto>,
)