package com.exalt.core.data.remote

import com.exalt.core.data.remote.api.WeatherApi
import com.exalt.core.data.remote.api.models.requests.CurrentWeatherRequest
import com.exalt.core.data.remote.api.models.requests.TimeStampWeatherRequest
import com.exalt.core.data.remote.api.models.responses.CurrentWeatherResponse
import com.exalt.core.data.remote.api.models.responses.WeatherTimeStampsResponse
import retrofit2.Response
import javax.inject.Inject

class WeatherRemoteDataSource @Inject constructor(
    private val weatherApi: WeatherApi
) {

    suspend fun getCurrentWeatherBy(currentWeatherRequest: CurrentWeatherRequest): Response<CurrentWeatherResponse> {
        return weatherApi.getCurrentWeatherByCity(currentWeatherRequest.toMap())
    }

    suspend fun getWeatherTimeStampsBy(weatherRequest: TimeStampWeatherRequest): Response<WeatherTimeStampsResponse> {
        return weatherApi.getWeatherTimeStampsByCity(weatherRequest.toMap())
    }

}