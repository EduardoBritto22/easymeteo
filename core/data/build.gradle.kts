import java.util.Properties
import java.io.FileInputStream

plugins {
    id("easyweather.android.library")
    id("easyweather.android.hilt")
    id("easyweather.android.room")
}

@Suppress("UnstableApiUsage")
android {
    namespace = "com.exalt.core.data"

    defaultConfig {

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")

        buildConfigField("String", "API_KEY", "")
    }

    buildTypes {
        debug {
            val apikeyPropertiesFile = rootProject.file("local.properties")
            val apikeyProperties = Properties()
            apikeyProperties.load(FileInputStream(apikeyPropertiesFile))

            buildConfigField("String", "API_KEY", apikeyProperties["API_KEY"].toString())
        }
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            buildConfigField("String", "API_KEY", "\"api\"")
        }
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
    }
}

dependencies {

    implementation(project(":core:domain"))

    // Retrofit
    implementation(libs.bundles.retrofit)
    
    // Coroutines
    implementation(libs.kotlinx.coroutines.android)

    // Time
    implementation(libs.joda)

    //Tests
    implementation(libs.kotlinx.coroutines.test)
    implementation(libs.bundles.unit.tests)

}