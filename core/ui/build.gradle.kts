plugins {
    id("easyweather.android.library")
    id("easyweather.android.library.compose")
}
@Suppress("UnstableApiUsage")
android {
    namespace = "com.exalt.core.ui"

    defaultConfig {
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)

    //Test
    implementation(libs.junit4)
}