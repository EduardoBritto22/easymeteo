package com.exalt.features.home.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.exalt.domain.models.City

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun CityRowItem(
    city: City,
    modifier: Modifier = Modifier,
    onClick: (City) -> Unit = {},
    onDeleteClick: (City) -> Unit = {}
) {
    var menuExpanded by remember { mutableStateOf(false) }

    Box {

        DropdownMenu(expanded = menuExpanded, onDismissRequest = { menuExpanded = false }) {
            DropdownMenuItem(text = { Text(text = "Delete") }, onClick = {
                menuExpanded = false
                onDeleteClick(city)
            })
        }

        Row(
            modifier = modifier
                .clip(shape = RoundedCornerShape(10.dp))
                .combinedClickable(onClick = {
                    onClick(city)
                },
                    onLongClick = {
                        menuExpanded = true
                    })
                .background(color = MaterialTheme.colorScheme.primaryContainer)
                .border(
                    border = BorderStroke(
                        width = 1.dp,
                        color = MaterialTheme.colorScheme.primary
                    ), shape = RoundedCornerShape(10.dp)
                )
                .padding(10.dp)
                .fillMaxWidth()
        ) {

            Column {
                Text(text = city.name, style = MaterialTheme.typography.labelLarge)
                Row {
                    city.stateCode?.let {
                        Text(
                            text = "${city.stateCode}, ",
                            style = MaterialTheme.typography.labelMedium
                        )
                    }
                    Text(text = city.countryCode, style = MaterialTheme.typography.labelMedium)
                }
            }
        }
    }

}

@Preview
@Composable
fun CityRowPreview() {
    CityRowItem(
        city = City(
            id = 0,
            name = "Paris",
            countryCode = "FR",
            stateCode = "Ile de France",
            sunrise = null,
            timezone = null,
            sunset = null,
            lon = null,
            lat = null
        )
    )
}