package com.exalt.features.home

import com.exalt.domain.models.City

sealed interface HomeUiState {
    data class Success(val city:List<City>) : HomeUiState
    object Error : HomeUiState
    object Loading : HomeUiState
}