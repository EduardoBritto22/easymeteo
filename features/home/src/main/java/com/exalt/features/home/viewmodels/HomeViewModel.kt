package com.exalt.features.home.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.exalt.domain.models.City
import com.exalt.domain.usecases.DeleteCityUseCase
import com.exalt.domain.usecases.GetCitiesListUseCase
import com.exalt.features.home.HomeUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getCitiesListUseCase: GetCitiesListUseCase,
    private val deleteCityUseCase: DeleteCityUseCase
): ViewModel() {


    private val _uiState: MutableStateFlow<HomeUiState> = MutableStateFlow(
        HomeUiState.Loading
    )

    val uiState get() = _uiState.asStateFlow()

    init {
        getCitiesList()
    }

    private fun getCitiesList() {
        viewModelScope.launch {
            getCitiesListUseCase().collect {
                _uiState.value = HomeUiState.Success(it)
            }
        }
    }

    fun remove(city: City) {
        viewModelScope.launch {
            deleteCityUseCase(city).onSuccess {
                getCitiesList()
            }
        }
    }

}