package com.exalt.features.home.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.exalt.core.ui.components.ErrorMessage
import com.exalt.core.ui.theme.EasyMeteoTheme
import com.exalt.domain.models.City
import com.exalt.features.home.HomeUiState
import com.exalt.features.home.HomeUiState.*
import com.exalt.features.home.components.CityRowItem
import com.exalt.core.ui.components.LoadingInfo
import com.exalt.features.home.viewmodels.HomeViewModel

@Composable
fun HomeScreenRoute(
    viewModel: HomeViewModel = hiltViewModel(),
    onCityClick: (City) -> Unit = {},
    navigateToCityForm: () -> Unit = {}
) {
    val uiState by viewModel.uiState.collectAsState()

    val onDeleteClick = { city: City ->
        viewModel.remove(city)
    }

    HomeScreen(uiState = uiState, navigateToCityForm, onCityClick, onDeleteClick)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun HomeScreen(
    uiState: HomeUiState,
    navigateToCityForm: () -> Unit = {},
    onCityClick: (City) -> Unit = {},
    onDeleteClick: (City) -> Unit = {}
) {

    Scaffold(
        topBar = {
            TopAppBar(
                title = { Text(text = "Easy Meteo") },
                colors = TopAppBarDefaults
                    .mediumTopAppBarColors(
                        containerColor = MaterialTheme.colorScheme.primary,
                        navigationIconContentColor = MaterialTheme.colorScheme.onPrimary,
                        titleContentColor = MaterialTheme.colorScheme.onPrimary
                    )
            )
        },
        floatingActionButton = {
            FloatingActionButton(
                onClick = navigateToCityForm,
            ) { Icon(Icons.Filled.Add, "Plus symbol") }

        }
    ) {
        Surface(
            modifier = Modifier
                .padding(it)
        ) {
            uiState.let { uiState ->
                when (uiState) {
                    is Success -> CitiesListContent(
                        uiState.city,
                        onClick = onCityClick,
                        onDeleteClick = onDeleteClick
                    )
                    is Error -> ErrorMessage("Unknown error")
                    Loading -> LoadingInfo()

                }
            }
        }
    }
}


@Composable
private fun CitiesListContent(
    citiesList: List<City>,
    onClick: (City) -> Unit,
    onDeleteClick: (City) -> Unit
) {
    LazyColumn(
        contentPadding = PaddingValues(5.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(5.dp),
        modifier = Modifier
            .fillMaxSize()
    ) {

        if (citiesList.isEmpty()) {
            item {
                Text(text = "Pas de Ville sauvegardée")
            }
        } else {
            items(items = citiesList, itemContent = { city ->
                CityRowItem(city = city, onClick = onClick, onDeleteClick = onDeleteClick)
            })
        }

    }
}

@Preview(showSystemUi = true)
@Composable
fun HomeScreenPreview() {
    EasyMeteoTheme {
        Surface(color = MaterialTheme.colorScheme.surface) {
            HomeScreen(
                uiState = Success(
                    listOf(
                        City(
                            id = 0,
                            name = "Belo Horizonte",
                            countryCode = "BR",
                            stateCode = "Minas Gerais",
                            sunrise = null,
                            timezone = null,
                            sunset = null,
                            lon = null,
                            lat = null
                        ),
                        City(
                            id = 0,
                            name = "Paris",
                            countryCode = "FR",
                            stateCode = null,
                            sunrise = null,
                            timezone = null,
                            sunset = null,
                            lon = null,
                            lat = null
                        )
                    )
                ),
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun HomeScreenErrorPreview() {
    EasyMeteoTheme {
        Surface {
            HomeScreen(
                uiState = Error,
            )
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun HomeScreenLoadingPreview() {
    EasyMeteoTheme {
        Surface {
            HomeScreen(
                uiState = Loading,
            )
        }
    }
}
