package com.exalt.features.home.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.exalt.domain.models.City
import com.exalt.features.home.screens.HomeScreenRoute

const val homeNavigationRoute = "home_route"

fun NavController.navigateToHome(){
    this.popBackStack(route = homeNavigationRoute, inclusive = false)
}

fun NavGraphBuilder.homeScreen(onCityClick: (City) -> Unit,navigateToCityForm: () -> Unit) {
    composable(homeNavigationRoute) {
        HomeScreenRoute(onCityClick = onCityClick, navigateToCityForm = navigateToCityForm)
    }
}
