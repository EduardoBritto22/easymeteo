package com.exalt.features.cityform.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.exalt.core.ui.components.EasyMeteoAppBar
import com.exalt.core.ui.theme.EasyMeteoTheme
import com.exalt.features.cityform.CityFormUIState
import com.exalt.features.cityform.components.FormTextField
import com.exalt.features.cityform.components.ValidateButton
import com.exalt.features.cityform.viewmodels.CityFormViewModel

@Composable
fun CityFormScreenRoute(
    viewModel: CityFormViewModel = hiltViewModel(),
    onBackClick: () -> Unit = {},
) {
    val uiState by viewModel.uiState.collectAsState()

    val onAddCityClick = {
        if (uiState.nameIsNotBlank() && uiState.countryIsNotBlank()) {
            viewModel.save()
            onBackClick()
        }
    }

    CityFormScreenRoute(uiState = uiState, onBackClick, onAddCityClick)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CityFormScreenRoute(
    uiState: CityFormUIState,
    onBackClick: () -> Unit,
    onAddCityClick: () -> Unit
) {
    Scaffold(
        topBar = {
            EasyMeteoAppBar("Easy Meteo", onBackClick)
        }
    ) {
        Surface(
            modifier = Modifier
                .padding(it)
        ) {
            CityFormContent(uiState, onAddCityClick)
        }
    }
}

@Composable
fun CityFormContent(uiState: CityFormUIState, onAddCityClick: () -> Unit) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(5.dp),
        modifier = Modifier.padding(5.dp)
    ) {

        FormTextField(
            text = uiState.name,
            onChange = uiState.onNameChange,
            placeholder = "Paris",
            label = "City name"
        )
        FormTextField(
            text = uiState.state,
            onChange = uiState.onStateChange,
            placeholder = "Ile de France",
            label = "State name"
        )
        FormTextField(
            text = uiState.countryCode,
            onChange = uiState.onCountryCodeChange,
            placeholder = "BR",
            label = "Country code"
        )

        ValidateButton(onAddCityClick)
    }
}



@Preview(showSystemUi = true)
@Composable
fun CityFormScreenPreview() {
    EasyMeteoTheme {
        Surface(color = MaterialTheme.colorScheme.surface) {
            CityFormScreenRoute(
                uiState = CityFormUIState(),
                onBackClick = {},
                onAddCityClick = {}
            )
        }
    }
}

