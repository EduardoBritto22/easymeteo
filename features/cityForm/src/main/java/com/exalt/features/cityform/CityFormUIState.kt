package com.exalt.features.cityform

data class CityFormUIState(
    val countryCode: String = "",
    val name: String = "",
    val state: String = "",
    val isCountryCodeError: Boolean = false,
    val onNameChange: (String) -> Unit = {},
    val onStateChange: (String) -> Unit = {},
    val onCountryCodeChange: (String) -> Unit = {},
) {
    fun nameIsNotBlank() = name.isNotBlank()
    fun countryIsNotBlank() = countryCode.isNotBlank() && !isCountryCodeError
}
