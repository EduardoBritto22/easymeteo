package com.exalt.features.cityform.components

import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
fun ValidateButton(onAddCityClick: () -> Unit) {
    ElevatedButton(onClick = onAddCityClick) {
        Text(text = "Add new City")
    }
}