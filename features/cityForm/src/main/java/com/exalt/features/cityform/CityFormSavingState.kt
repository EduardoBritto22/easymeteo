package com.exalt.features.cityform

import com.exalt.domain.models.City

sealed interface CityFormSavingState {
    data class Success(val city: City) : CityFormSavingState
    object Error : CityFormSavingState
    object Loading : CityFormSavingState
}