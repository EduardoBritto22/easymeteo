package com.exalt.features.cityform.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.exalt.domain.models.City
import com.exalt.domain.usecases.SaveCityUseCase
import com.exalt.features.cityform.CityFormSavingState
import com.exalt.features.cityform.CityFormUIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CityFormViewModel @Inject constructor(
    private val saveCityUseCase: SaveCityUseCase
) : ViewModel() {


    private val _validatingState: MutableStateFlow<CityFormSavingState> = MutableStateFlow(
        CityFormSavingState.Loading
    )

    private val _uiState: MutableStateFlow<CityFormUIState> = MutableStateFlow(
        CityFormUIState()
    )

    val validatingState get() = _validatingState.asStateFlow()
    val uiState get() = _uiState.asStateFlow()

    init {
        _uiState.update { currentState ->
            currentState.copy(
                onNameChange = {
                    _uiState.value = _uiState.value.copy(
                        name = it,
                    )
                },
                onCountryCodeChange = {
                    val isCountryCodeError = (it.length > 3)

                    _uiState.value = _uiState.value.copy(
                        countryCode = it,
                    )

                    _uiState.value = _uiState.value.copy(
                        isCountryCodeError = isCountryCodeError,
                    )

                },
                onStateChange = {
                    _uiState.value = _uiState.value.copy(
                        state = it,
                    )
                }

            )
        }

    }


    fun save() {
        _uiState.value.run {
            val city = City(
                id = 0,
                name = name.trim(),
                countryCode = countryCode.trim(),
                stateCode = state.trim().ifEmpty { null },
            )
            viewModelScope.launch {
                _validatingState.value = CityFormSavingState.Loading

                saveCityUseCase(city).fold(
                    onSuccess = {
                        _validatingState.value = CityFormSavingState.Success(city)
                    },
                    onFailure = {
                        _validatingState.value = CityFormSavingState.Error
                    }
                )
            }
        }


    }

}