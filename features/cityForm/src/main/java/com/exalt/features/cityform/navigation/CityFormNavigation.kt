package com.exalt.features.cityform.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavOptions
import androidx.navigation.compose.composable
import com.exalt.features.cityform.screens.CityFormScreenRoute

const val cityFormNavigationRoute = "city_form_route"

fun NavController.navigateToCityForm(navOptions: NavOptions? = null) {
    this.navigate(cityFormNavigationRoute, navOptions)
}


fun NavGraphBuilder.cityForm(onBackClick: () -> Unit = {}) {
    composable(cityFormNavigationRoute) {
        CityFormScreenRoute(onBackClick = onBackClick)
    }
}