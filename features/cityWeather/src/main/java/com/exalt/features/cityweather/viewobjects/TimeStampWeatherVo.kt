package com.exalt.features.cityweather.viewobjects

import androidx.annotation.DrawableRes

data class TimeStampWeatherVo(
    val timeStamp: String,
    val temperature: String,
    val humidity: String,
    @DrawableRes
    val mainWeatherIcon: Int?,
    val windDirectionDeg: Int,
    val windSpeed: String,
    val precipitationProbability: String
)