package com.exalt.features.cityweather.screens

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.exalt.core.ui.components.EasyMeteoAppBar
import com.exalt.core.ui.components.ErrorMessage
import com.exalt.core.ui.theme.EasyMeteoTheme
import com.exalt.features.cityweather.components.SecondaryWeatherData
import com.exalt.features.cityweather.components.TimeStampsWeatherContent
import com.exalt.features.cityweather.components.WeatherDataHeader
import com.exalt.features.cityweather.viewmodels.CityWeatherViewModel
import com.exalt.features.cityweather.viewobjects.CityWeatherUiState
import com.exalt.features.cityweather.viewobjects.CityWeatherVo
import com.exalt.features.cityweather.viewobjects.TimeStampWeatherVo
import com.exalt.core.ui.components.LoadingInfo
import org.joda.time.LocalDateTime

@Composable
fun CityWeatherScreenRoute(
    viewModel: CityWeatherViewModel = hiltViewModel(),
    onBackClick: () -> Unit = {},
) {
    val uiState by viewModel.uiState.collectAsState()
    val cityName:String = viewModel.cityName

    CityWeatherScreen(cityName = cityName, uiState = uiState, onBackClick)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CityWeatherScreen(
    cityName: String,
    uiState: CityWeatherUiState,
    onBackClick: () -> Unit
) {
    Scaffold(
        topBar = {
            EasyMeteoAppBar(cityName, onBackClick)
        }
    ) {
        Surface(
            modifier = Modifier
                .padding(it)
        ) {

            when (uiState) {
                is CityWeatherUiState.Success -> MainWeatherContent(uiState.weatherData)
                is CityWeatherUiState.Error -> ErrorMessage("Error loading the data")
                CityWeatherUiState.Loading -> LoadingInfo()

            }
        }
    }
}


@Composable
fun MainWeatherContent(weatherData: CityWeatherVo) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(10.dp),
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp),
    ) {

        WeatherDataHeader(weatherData)

        TimeStampsWeatherContent(weatherData.timeStampsList)

        SecondaryWeatherData(weatherData)
    }
}


@Preview(showSystemUi = true)
@Composable
fun CityFormScreenPreview() {
    EasyMeteoTheme {
        Surface(color = MaterialTheme.colorScheme.surface) {
            CityWeatherScreen(
                cityName = "Paris",
                uiState = CityWeatherUiState.Success(
                    CityWeatherVo(
                        time = LocalDateTime.now().toString("hh:mm"),
                        temperature = "50°",
                        feelsLike = "55°",
                        windSpeed =  "30.0km/h",
                        humidity = "20%",
                        windDirectionDeg = 20,
                        mainWeatherIcon = com.exalt.core.ui.R.drawable.ic_10d,
                        mainWeatherDescription = "Rain",
                        sunset = "18:30",
                        sunrise = "06:50",
                        timeStampsList = timeStampWeatherData
                    )
                ),
                onBackClick = {},
            )
        }
    }
}

private val timeStampWeatherData = listOf(
    TimeStampWeatherVo(
        precipitationProbability = "100%",
        timeStamp = "15h30",
        temperature = "50°",
        windSpeed = "21.0km/h",
        mainWeatherIcon = com.exalt.core.ui.R.drawable.ic_10d,
        humidity = "20%",
        windDirectionDeg = 20
    ),
    TimeStampWeatherVo(
        precipitationProbability = "100%",
        timeStamp = "15h30",
        temperature = "50°",
        windSpeed = "22.0km/h",
        mainWeatherIcon = com.exalt.core.ui.R.drawable.ic_10d,
        humidity = "20%",
        windDirectionDeg = 20
    ),
    TimeStampWeatherVo(
        precipitationProbability = "100%",
        timeStamp = "15h30",
        temperature = "50°",
        windSpeed = "23.0km/h",
        mainWeatherIcon = com.exalt.core.ui.R.drawable.ic_10d,
        humidity = "20%",
        windDirectionDeg = 20
    ),
    TimeStampWeatherVo(
        precipitationProbability = "100%",
        timeStamp = "15h30",
        temperature = "50°",
        windSpeed = "24.0km/h",
        mainWeatherIcon = com.exalt.core.ui.R.drawable.ic_10d,
        humidity = "20%",
        windDirectionDeg = 20
    ),
)

