package com.exalt.features.cityweather.mappers

import android.content.res.Resources
import com.exalt.domain.models.MainWeather
import com.exalt.domain.models.TimeStampWeatherData
import com.exalt.domain.models.WeatherData
import com.exalt.features.cityweather.viewobjects.CityWeatherVo
import com.exalt.features.cityweather.viewobjects.TimeStampWeatherVo
import org.joda.time.LocalDateTime
import javax.inject.Inject
import kotlin.math.roundToInt

class CurrentWeatherVoMapper @Inject constructor(
    private val resources: Resources,
    private val packageName: String
) {

    fun toVO(modelData: WeatherData, timeStamps: List<TimeStampWeatherData>): CityWeatherVo = CityWeatherVo(
        temperature = formatToTemperatureView(modelData.temperature),
        humidity = formatToHumidityView(modelData.humidity),
        feelsLike = formatToTemperatureView(modelData.feelsLike),
        time = formatToTimeFormatView(modelData.time),
        windSpeed = formatToWindSpeedView(modelData.windSpeed),
        windDirectionDeg = modelData.windDirectionDeg,
        mainWeatherDescription = getFirstMainWeatherDescription(modelData.mainWeather),
        mainWeatherIcon = getFirstMainWeatherIcon(modelData.mainWeather.firstOrNull()?.mainWeatherIcon),
        sunrise = "",
        sunset = "",
        timeStampsList = timeStamps.map { toTimeStampWeatherVO(it) }
    )

    private fun toTimeStampWeatherVO(timeStampWeatherData: TimeStampWeatherData) =
        TimeStampWeatherVo(
            temperature = formatToTemperatureView(timeStampWeatherData.temperature),
            humidity = formatToHumidityView(timeStampWeatherData.humidity),
            timeStamp = formatToTimeFormatView(timeStampWeatherData.timeStamp),
            windSpeed = formatToWindSpeedView(timeStampWeatherData.windSpeed),
            windDirectionDeg = timeStampWeatherData.windDirectionDeg,
            mainWeatherIcon = getFirstMainWeatherIcon(timeStampWeatherData.mainWeatherIcon),
            precipitationProbability = formatToPrecipitationProbabilityView(timeStampWeatherData.precipitationProbability),
        )

    private fun formatToTemperatureView(value: Double): String = "${value.roundToInt()}°"
    private fun formatToHumidityView(value: Int): String = "${value}%"
    private fun formatToWindSpeedView(value: Double): String = "${value.roundToInt()}km/h"
    private fun formatToPrecipitationProbabilityView(value: Double): String = "${value*100}%"
    private fun formatToTimeFormatView(value: LocalDateTime): String = value.toString("HH:mm")

    private fun getFirstMainWeatherDescription(mainWeatherList: List<MainWeather>): String =
        mainWeatherList.firstOrNull()?.mainWeatherDescription.orEmpty()

    private fun getFirstMainWeatherIcon(icon: String?): Int? {

        if (icon != null) {
            val iconName = "ic_$icon"

            return resources.getIdentifier(
                iconName,
                "drawable",
                packageName
            )
        }
        return null
    }


}


