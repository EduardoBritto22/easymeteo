package com.exalt.features.cityweather.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.exalt.features.cityweather.viewobjects.CityWeatherVo

@Composable
fun WeatherDataHeader(weatherData: CityWeatherVo) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .heightIn(50.dp, 200.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = weatherData.temperature, style = MaterialTheme.typography.displaySmall)
            Column {
                Text(text = "felt: ${weatherData.feelsLike}", style = MaterialTheme.typography.bodyMedium)
                Text(text = weatherData.mainWeatherDescription, style = MaterialTheme.typography.bodyMedium)

            }

            weatherData.mainWeatherIcon?.let { icon ->
                Image(
                    painterResource(icon),
                    "content description",
                    contentScale = ContentScale.FillBounds,

                    modifier = Modifier.clipToBounds()
                )
            }
        }
    }
}
