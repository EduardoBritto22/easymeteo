package com.exalt.features.cityweather.components

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.exalt.core.ui.R
import com.exalt.features.cityweather.viewobjects.CityWeatherVo

@Composable
fun SecondaryWeatherData(weatherData: CityWeatherVo) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(10.dp),
    ) {
        SecondaryWeatherDataItem(
            label = "Humidity",
            value = weatherData.humidity,
            icon = R.drawable.ic_precipitation
        )

        SecondaryWeatherDataItem(
            label = "Wind",
            value = weatherData.windSpeed,
            icon = R.drawable.ic_wind
        )
    }
}

@Composable
private fun SecondaryWeatherDataItem(
    label: String,
    value: String,
    @DrawableRes icon: Int
) {
    BoxWithConstraints(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .size(64.dp)
            .clip(shape = RoundedCornerShape(10.dp))
            .background(color = MaterialTheme.colorScheme.primaryContainer)
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Icon(
                painter = painterResource(icon),
                tint = MaterialTheme.colorScheme.onSurface,
                contentDescription = "weather data icon"
            )
            Text(text = label, style = MaterialTheme.typography.labelMedium)
            Text(text = value, style = MaterialTheme.typography.bodySmall)
        }
    }
}

