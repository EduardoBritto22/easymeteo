package com.exalt.features.cityweather.viewmodels

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.exalt.domain.usecases.GetCurrentWeatherUseCase
import com.exalt.domain.usecases.GetTimeStampsWeatherUseCase
import com.exalt.features.cityweather.mappers.CurrentWeatherVoMapper
import com.exalt.features.cityweather.navigation.CityWeatherArgs
import com.exalt.features.cityweather.viewobjects.CityWeatherUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CityWeatherViewModel @Inject constructor(
    private val getCurrentWeatherUseCase: GetCurrentWeatherUseCase,
    private val getTimeStampsWeatherUseCase: GetTimeStampsWeatherUseCase,
    private val weatherVoMapper: CurrentWeatherVoMapper,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val timeStamps = 8 // 8 timestamps of 3h : 24h

    private val cityWeatherArgs = CityWeatherArgs(savedStateHandle)

    val cityName: String
        get() = cityWeatherArgs.cityName


    private val _uiState: MutableStateFlow<CityWeatherUiState> = MutableStateFlow(
        CityWeatherUiState.Loading
    )

    val uiState get() = _uiState.asStateFlow()

    init {
        val city = cityWeatherArgs.convertToCity()//City(id = 0,name = cityName, stateCode = state, countryCode = countryCode)
        viewModelScope.launch {
            getCurrentWeatherUseCase(city).collect { resultCurrentWeather ->
                resultCurrentWeather.fold(
                    onSuccess = { weatherData ->
                        getTimeStampsWeatherUseCase(city, timeStamps).collect { resultTimeStamps ->
                            resultTimeStamps.fold(
                                onSuccess = { timeStamps ->
                                    _uiState.value = CityWeatherUiState.Success(weatherVoMapper.toVO(weatherData, timeStamps))
                                },
                                onFailure = {
                                    _uiState.value = CityWeatherUiState.Error
                                }
                            )
                        }
                    },
                    onFailure = {
                        _uiState.value = CityWeatherUiState.Error
                    }
                )
            }
        }
    }


}