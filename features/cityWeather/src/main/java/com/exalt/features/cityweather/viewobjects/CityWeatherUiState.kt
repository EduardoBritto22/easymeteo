package com.exalt.features.cityweather.viewobjects

sealed interface CityWeatherUiState {

    data class Success(val weatherData: CityWeatherVo) : CityWeatherUiState
    object Error : CityWeatherUiState
    object Loading : CityWeatherUiState

}