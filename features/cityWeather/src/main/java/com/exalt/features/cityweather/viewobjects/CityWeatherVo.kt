package com.exalt.features.cityweather.viewobjects

import androidx.annotation.DrawableRes

data class CityWeatherVo(
    val temperature: String,
    val feelsLike: String,
    val humidity: String,
    val time: String,
    val windDirectionDeg: Int,
    val windSpeed: String,
    val sunset: String?,
    val sunrise: String?,
    @DrawableRes
    val mainWeatherIcon: Int?,
    val mainWeatherDescription: String,
    val timeStampsList: List<TimeStampWeatherVo>
)