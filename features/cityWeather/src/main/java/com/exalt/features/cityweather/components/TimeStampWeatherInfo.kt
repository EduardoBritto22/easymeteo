package com.exalt.features.cityweather.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.exalt.features.cityweather.viewobjects.TimeStampWeatherVo


@Composable
fun TimeStampWeatherInfo(timeStampWeatherData: TimeStampWeatherVo) {
    Column(
        verticalArrangement = Arrangement.spacedBy(5.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(timeStampWeatherData.timeStamp, style = MaterialTheme.typography.bodySmall)

        timeStampWeatherData.mainWeatherIcon?.let { icon ->
            Image(
                painterResource(icon),
                "content description",
                contentScale = ContentScale.FillBounds,
            )
        }

        Text(text = "¤", style = MaterialTheme.typography.bodySmall)

        Text(text = timeStampWeatherData.temperature, style = MaterialTheme.typography.bodySmall)

        Text(text = timeStampWeatherData.precipitationProbability, style = MaterialTheme.typography.bodySmall)
    }
}
