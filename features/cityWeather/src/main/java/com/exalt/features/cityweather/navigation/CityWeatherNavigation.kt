package com.exalt.features.cityweather.navigation

import androidx.lifecycle.SavedStateHandle
import androidx.navigation.*
import androidx.navigation.compose.composable
import com.exalt.domain.models.City
import com.exalt.features.cityweather.screens.CityWeatherScreenRoute

const val cityWeatherNavigationRoute = "city_weather_route"

private const val cityNameArg = "cityName"
private const val countryCodeArg = "countryCode"
private const val stateNameArg = "state"

internal class CityWeatherArgs(val cityName: String, private val countryCode: String, private val stateName: String?) {
    constructor(savedStateHandle: SavedStateHandle) :
            this(
                checkNotNull(savedStateHandle[cityNameArg]) as String,
                checkNotNull(savedStateHandle[countryCodeArg]) as String,
                savedStateHandle[stateNameArg]
            )
    fun convertToCity() = City(
        id = 0,
        name = cityName,
        stateCode = stateName,
        countryCode = countryCode
    )
}

fun NavController.navigateToCityWeather(city: City, navOptions: NavOptions? = null) {
    this.navigate("$cityWeatherNavigationRoute/${city.name}/?state=${city.stateCode}/${city.countryCode}", navOptions)
}

fun NavGraphBuilder.cityWeather(onBackClick: () -> Unit) {
    composable(
        "$cityWeatherNavigationRoute/{cityName}/?state={state}/{countryCode}", arguments = listOf(
            navArgument(cityNameArg) { type = NavType.StringType },
            navArgument(countryCodeArg) { type = NavType.StringType },
            navArgument(stateNameArg) { type = NavType.StringType },
        )
    ) {
        CityWeatherScreenRoute(onBackClick = onBackClick)
    }
}