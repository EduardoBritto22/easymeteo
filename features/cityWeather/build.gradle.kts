plugins {
    id("easyweather.android.feature")
    id("easyweather.android.library.compose")
}
@Suppress("UnstableApiUsage")
android {
    namespace = "com.exalt.features.cityweather"
    defaultConfig {
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.lifecycle.ktx)

    // Time
    implementation(libs.joda)

}