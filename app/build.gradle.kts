plugins {
    id("easyweather.android.application")
    id("easyweather.android.application.compose")
    id("easyweather.android.hilt")
}

@Suppress("UnstableApiUsage")
android {
    val major = 1
    val minor = 0
    val fix = 0
    val VERSION_CODE = major * 1_000_000 + minor * 1_000 + fix
    val VERSION_NAME = "$major.$minor.$fix"

    namespace = "com.exalt.easymeteo"

    defaultConfig {
        applicationId = "com.exalt.easymeteo"
        versionCode = VERSION_CODE
        versionName = VERSION_NAME

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }


    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {

    implementation(project(":core:ui"))
    implementation(project(":core:data"))
    implementation(project(":core:domain"))
    implementation(project(":features:home"))
    implementation(project(":features:cityForm"))
    implementation(project(":features:cityWeather"))

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.lifecycle.ktx)
    implementation(libs.androidx.activity.compose)
    implementation(libs.androidx.navigation.compose)

    //Test
    debugImplementation(libs.junit4)
    debugImplementation(libs.androidx.compose.ui.tooling)
    debugImplementation(libs.androidx.compose.ui.test.manifest)
}