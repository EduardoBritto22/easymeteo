package com.exalt.easymeteo.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.exalt.features.cityform.navigation.cityForm
import com.exalt.features.cityform.navigation.navigateToCityForm
import com.exalt.features.cityweather.navigation.cityWeather
import com.exalt.features.cityweather.navigation.navigateToCityWeather
import com.exalt.features.home.navigation.homeNavigationRoute
import com.exalt.features.home.navigation.homeScreen
import com.exalt.features.home.navigation.navigateToHome

@Composable
fun EasyWeatherNavHost(
    navController: NavHostController,
    modifier: Modifier = Modifier,
    startDestination: String = homeNavigationRoute,
) {
    NavHost(navController, startDestination = startDestination, modifier = modifier) {

        cityForm(onBackClick = navController::navigateToHome)

        cityWeather(onBackClick = navController::navigateToHome)

        homeScreen(
            onCityClick = navController::navigateToCityWeather,
            navigateToCityForm = navController::navigateToCityForm
        )
    }
}